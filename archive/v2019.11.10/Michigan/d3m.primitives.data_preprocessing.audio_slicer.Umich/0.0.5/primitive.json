{
    "id": "a0a60c4c-4e69-3ebe-81c9-7432c5b41c38",
    "version": "0.0.5",
    "name": "AudioSlicer",
    "description": "Audio utility for splitting audio data stored in a numpy array into\nequal-length clips with optional overlap\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "keywords": [
        "feature_extraction",
        "audio"
    ],
    "source": {
        "name": "Michigan",
        "contact": "mailto:davjoh@umich.edu",
        "uris": [
            "https://github.com/dvdmjohnson/d3m_michigan_primitives/blob/master/spider/featurization/audio_slicer/audio_slicer.py",
            "https://github.com/dvdmjohnson/d3m_michigan_primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/dvdmjohnson/d3m_michigan_primitives.git@bd1d461348cbc9a121bd4f13a15b3659e3e2d926#egg=spider"
        },
        {
            "type": "UBUNTU",
            "package": "ffmpeg",
            "version": "7:2.8.11-0ubuntu0.16.04.1"
        }
    ],
    "python_path": "d3m.primitives.data_preprocessing.audio_slicer.Umich",
    "hyperparams_to_tune": [
        "frame_length"
    ],
    "algorithm_types": [
        "AUDIO_STREAM_MANIPULATION"
    ],
    "primitive_family": "DATA_PREPROCESSING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "spider.featurization.audio_slicer.audio_slicer.AudioSlicer",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.list.List",
            "Outputs": "d3m.container.list.List",
            "Hyperparams": "spider.featurization.audio_slicer.audio_slicer.AudioSlicerHyperparams",
            "Params": "NoneType"
        },
        "interfaces_version": "2019.11.10",
        "interfaces": [
            "featurization.FeaturizationTransformerPrimitiveBase",
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "sampling_rate": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 44100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "uniform sampling rate of the audio data",
                "lower": 1,
                "upper": 192000,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "frame_length": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.05,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "duration in seconds that defines the length of the audio processing window",
                "lower": 0.0,
                "upper": 100.0,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "overlap": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.025,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "duration in seconds that defines the step size taken along the time series during subsequent processing steps",
                "lower": 0.0,
                "upper": 99.99,
                "lower_inclusive": true,
                "upper_inclusive": true
            },
            "pad": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": 1,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "indicates whether clips shorter than frame_length should be padded with zeros to a duration of exactly frame_length",
                "values": [
                    0,
                    1
                ]
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "spider.featurization.audio_slicer.audio_slicer.AudioSlicerHyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "docker_containers": {
                "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.list.List",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "NoneType",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "spider.featurization.audio_slicer.audio_slicer.AudioSlicerHyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]"
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "returns": "NoneType",
                "description": "Audio utility for splitting audio data stored in a numpy array into\nequal-length clips with optional overlap"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "A noop.\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Splits each audio file into slices. Each file is represented in the\ninput list as an ndarray. The output list contains the same number of\nelements, with each element having a number of rows determined by the\nclip length and the hyperparameters frame_length, overlap, and pad.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        }
    },
    "structural_type": "spider.featurization.audio_slicer.audio_slicer.AudioSlicer",
    "digest": "41c84b56236546a7addcb316536f0780e53d98ff121f606d7322dc61bcde6985"
}
